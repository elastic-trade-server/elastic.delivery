# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-29 17:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: api.py:43 models.py:33
msgid "Code"
msgstr "Код"

#: api.py:45 models.py:34
msgid "Counterparty"
msgstr "Контрагент"

#: api.py:48 api.py:51 models.py:62
msgid "Available payment methods"
msgstr "Доступные методы оплаты"

#: api.py:54 models.py:97
msgid "Delivery areas"
msgstr "Зоны доставки"

#: api.py:85 models.py:69
msgid "Method"
msgstr "Метод"

#: api.py:86 api.py:101 api.py:119 api.py:138 models.py:30 models.py:70
#: models.py:87 models.py:104 models.py:123
msgid "Carrier"
msgstr "Возчик"

#: api.py:99 models.py:83
msgid "Max weight cargo"
msgstr "Макс. вес груза"

#: api.py:100 models.py:79 models.py:86
msgid "Delivery cost"
msgstr "Стоимость доставки"

#: api.py:114 models.py:99 models.py:248
msgid "Name"
msgstr "Наименование"

#: api.py:115 models.py:100
msgid "Distance"
msgstr "Дистанция"

#: api.py:116 models.py:101
msgid "Markup"
msgstr "Наценка"

#: api.py:117
msgid "Time"
msgstr "Время"

#: api.py:118 models.py:103
msgid "Description"
msgstr "Описание"

#: api.py:132 models.py:118
msgid "Document number"
msgstr "Номер документа"

#: api.py:133 models.py:119
msgid "Creation date"
msgstr "Дата создания"

#: api.py:134 models.py:120
msgid "Order"
msgstr "Счет"

#: api.py:136 models.py:121
msgid "Consignee"
msgstr "Грузо-получатель"

#: api.py:137 models.py:122
msgid "Payment method"
msgstr "Метод оплаты"

#: api.py:141 models.py:202
msgid "Delivery term"
msgstr "Условия доставки"

#: api.py:169 models.py:232
msgid "INCOTERMS code"
msgstr "Код ИНКОТЕРМС"

#: api.py:171 models.py:233
msgid "Alternative delivery terms code"
msgstr "Альтернативный код доставки"

#: api.py:174
msgid "Addresses collection"
msgstr "Коллекция адресов"

#: api.py:176 models.py:236
msgid "Delivery instruction"
msgstr "Инструкция по доставке"

#: api.py:178 models.py:115 models.py:237
msgid "Invoice"
msgstr "Инвойс"

#: models.py:31
msgid "Carriers"
msgstr "Возчики"

#: models.py:61
msgid "Available payment method"
msgstr "Доступный метод оплаты"

#: models.py:65
msgid "Prepayment"
msgstr "Предопата"

#: models.py:66
msgid "Cash on delivery"
msgstr "Наложный платеж"

#: models.py:80
msgid "Delivery costs"
msgstr "Стоимости доставки"

#: models.py:84
msgid "In kilograms"
msgstr "В килограммах"

#: models.py:96
msgid "Delivery area"
msgstr "Зона доставки"

#: models.py:100
msgid "In kilometers"
msgstr "В километрах"

#: models.py:102
msgid "Time delivery"
msgstr "Время доставки"

#: models.py:102
msgid "In hours"
msgstr "В часах"

#: models.py:116
msgid "Invoices"
msgstr "Инвойсы"

#: models.py:203
msgid "Delivery terms"
msgstr "Условия доставки"

#: models.py:206
msgid "Ex worker"
msgstr "Франко-завод"

#: models.py:208
msgid "Free carrier"
msgstr "Франко-перевозчик"

#: models.py:211
msgid "Carriage paid to"
msgstr ""

#: models.py:215
msgid "Carriage and insurance paid to"
msgstr ""

#: models.py:216
msgid "Delivered at terminal"
msgstr "Доставка до терминала"

#: models.py:220
msgid "Delivered at point"
msgstr "Доставка в место назначения"

#: models.py:222
msgid "Delivered duty paid"
msgstr ""

#: models.py:224
msgid "Free on board"
msgstr "Отгрузка на судно"

#: models.py:225
msgid "Free alongside ship"
msgstr ""

#: models.py:227
msgid "Cost and freight"
msgstr "Стоимость и фрахт"

#: models.py:229
msgid "Cost, Insurance and Freight"
msgstr "Стоимость, страхование и фрахт"

#: models.py:235
msgid "Delivery location"
msgstr "Место назначения"

#: models.py:245
msgid "Sequence"
msgstr "Последовательность"

#: models.py:246
msgid "Sequences"
msgstr "Последовательности"

#: models.py:249
msgid "Separator"
msgstr "Разделитель"

#: models.py:250
msgid "Prefix"
msgstr "Префикс"

#: models.py:251
msgid "Number"
msgstr "Номер"

#: models.py:252
msgid "Suffix"
msgstr "Суффикс"
