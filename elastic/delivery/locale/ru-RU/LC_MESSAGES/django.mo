��    3      �  G   L      h     i     ~     �     �     �     �     �     �  	   �               /     <     J     ]     s     �     �     �     �     �     �     �     �     �       	              -     ;     J     S     `     n     v          �     �     �     �     �     �     �  
   �  	   �     �  	   �     �     �     �  �    !   �  4   �  *   	  ,   @	     m	     z	     �	     �	     �	      �	  8   �	     (
     =
  3   W
  (   �
     �
     �
  #   �
  #     *   0     [     {     �     �     �     �     �  !         7     X     r     �     �     �     �     �     �  
   �       
        (     1     I     X     k  $   �  $   �     �  
   �     �            )             3           #       1   
      0            .      $   (                        *   	                   !         2                 "   /                       ,           -   '             +      &                         %        Addresses collection Alternative delivery terms code Available payment method Available payment methods Carrier Carriers Cash on delivery Code Consignee Cost and freight Cost, Insurance and Freight Counterparty Creation date Delivered at point Delivered at terminal Delivery area Delivery areas Delivery cost Delivery costs Delivery instruction Delivery location Delivery term Delivery terms Description Distance Document number Ex worker Free carrier Free on board INCOTERMS code In hours In kilograms In kilometers Invoice Invoices Markup Max weight cargo Method Name Number Order Payment method Prefix Prepayment Separator Sequence Sequences Suffix Time Time delivery Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-04-29 17:04+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Коллекция адресов Альтернативный код доставки Доступный метод оплаты Доступные методы оплаты Возчик Возчики Наложный платеж Код Грузо-получатель Стоимость и фрахт Стоимость, страхование и фрахт Контрагент Дата создания Доставка в место назначения Доставка до терминала Зона доставки Зоны доставки Стоимость доставки Стоимости доставки Инструкция по доставке Место назначения Условия доставки Условия доставки Описание Дистанция Номер документа Франко-завод Франко-перевозчик Отгрузка на судно Код ИНКОТЕРМС В часах В килограммах В километрах Инвойс Инвойсы Наценка Макс. вес груза Метод Наименование Номер Счет Метод оплаты Префикс Предопата Разделитель Последовательность Последовательности Суффикс Время Время доставки 