# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from django.utils.translation import ugettext_lazy as _
from tastypie.authorization import Authorization
from django.conf.urls import url
from tastypie.bundle import Bundle
from tastypie.utils.urls import trailing_slash
import json
from django.core.serializers.json import DjangoJSONEncoder
from tastypie.serializers import Serializer
from tastypie.exceptions import Unauthorized


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Delivery package"


class DojoRefSerializer(Serializer):
    def to_json(self, data, options=None):
        options = options or {}

        data = self.to_simple(data, options)

        if 'objects' in data:
            data = data['objects']

        return json.dumps(data, cls=DjangoJSONEncoder, sort_keys=True)


class CustomAuthorization(Authorization):

    @staticmethod
    def base_checks(bundle):
        raise NotImplemented

    @staticmethod
    def base_filters(bundle, object_list):
        raise NotImplemented

    def read_list(self, object_list, bundle):
        return self.base_filters(bundle, object_list)

    def read_detail(self, object_list, bundle):
        try:
            return self.base_checks(bundle)
        except Exception:
            raise Unauthorized("You are not allowed to access that resource.")

    def create_list(self, object_list, bundle):
        return self.base_filters(bundle, object_list)

    def create_detail(self, object_list, bundle):
        try:
            return self.base_checks(bundle)
        except Exception:
            raise Unauthorized("You are not allowed to access that resource.")

    def update_list(self, object_list, bundle):
        return self.base_filters(bundle, object_list)

    def update_detail(self, object_list, bundle):
        try:
            return self.base_checks(bundle)
        except Exception:
            raise Unauthorized("You are not allowed to access that resource.")

    def delete_list(self, object_list, bundle):
        return self.base_filters(bundle, object_list)

    def delete_detail(self, object_list, bundle):
        try:
            return self.base_checks(bundle)
        except Exception:
            raise Unauthorized("You are not allowed to access that resource.")


class CarrierResource(ModelResource):
    class Meta:
        queryset = Carrier.objects.all()
        resource_name = 'carrier'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', ]
        include_resource_uri = False
        excludes = ['id', ]
        serializer = DojoRefSerializer()
        filtering = {
            "code": ALL,
            "party": ALL_WITH_RELATIONS,
            "payment_methods": ALL_WITH_RELATIONS,
        }

    code = fields.CharField(attribute='code', help_text=_('Code'))
    party = fields.ToOneField("elastic.party.api.PartyResource", attribute='party', full=True, help_text=_('Party'))
    payment_methods = fields.ToManyField(to='elastic.delivery.api.AvailablePaymentMethodResource',
                                         attribute='payment_methods', null=True, blank=True, full=False,
                                         related_name='carrier', help_text=_('Available payment methods'))
    delivery_price_list = fields.ToManyField(to='elastic.delivery.api.DeliveryCostResource',
                                             attribute='delivery_price_list', null=True, blank=True, full=False,
                                             related_name='carrier', help_text=_('Available payment methods'))
    delivery_areas = fields.ToManyField(to='elastic.delivery.api.DeliveryAreaResource',
                                        attribute='delivery_areas', null=True, blank=True, full=False,
                                        related_name='carrier', help_text=_('Delivery areas'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs


class AvailablePaymentMethodResource(ModelResource):
    class Meta:
        queryset = AvailablePaymentMethod.objects.all()
        resource_name = 'payment_methods'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', ]
        excludes = ['id', ]

    method = fields.CharField(attribute='method', help_text=_('Method'))
    carrier = fields.ToOneField(to=CarrierResource, full=False, attribute='carrier', help_text=_('Carrier'))


class DeliveryCostResource(ModelResource):
    class Meta:
        queryset = DeliveryCost.objects.all()
        resource_name = 'delivery_cost'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', ]
        excludes = ['id', ]

    max_weight_cargo = fields.DecimalField(attribute='max_weight_cargo', help_text=_('Max weight cargo'))
    delivery_cost = fields.DecimalField(attribute='delivery_cost', help_text=_('Delivery cost'))
    carrier = fields.ToOneField(to=CarrierResource, full=False, attribute='carrier', help_text=_('Carrier'))


class DeliveryAreaResource(ModelResource):
    class Meta:
        queryset = DeliveryCost.objects.all()
        resource_name = 'delivery_area'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', ]
        excludes = ['id', ]

    name = fields.CharField(attribute='name', help_text=_('Name'))
    distance = fields.IntegerField(attribute='distance', help_text=_('Distance'))
    markup = fields.DecimalField(attribute='markup', help_text=_('Markup'))
    time = fields.IntegerField(attribute='time', help_text=_('Time'))
    description = fields.CharField(attribute='description', help_text=_('Description'))
    carrier = fields.ToOneField(to=CarrierResource, full=False, attribute='carrier', help_text=_('Carrier'))
