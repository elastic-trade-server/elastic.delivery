# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.db import models
from elastic.party.models import Party
from django.db import transaction
import math
from elastic.product.models import Product
from django.core.exceptions import ObjectDoesNotExist
from decimal import Decimal


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Delivery package"


class Carrier(models.Model):
    """
    Организация-возчик
    """

    class Meta:
        verbose_name = _('Carrier')
        verbose_name_plural = _('Carriers')

    #: Справочный код
    code = models.CharField(max_length=64, unique=True, db_index=True, verbose_name=_('Code'))

    #: Особа
    party = models.ForeignKey(Party, related_name='carriers', verbose_name=_('Party'))

    @staticmethod
    def calculate_distance(address_store, address_consignee):
        latitude = address_consignee.ymaps.all()[0].latitude - address_store.ymaps.all()[0].latitude
        longitude = address_consignee.ymaps.all()[0].longitude - address_store.ymaps.all()[0].longitude

        # x = 6371 * (math.pi/180) * math.cos(math.fabs(latitude) * math.pi / 180)
        x = (40000/360) * math.cos(latitude) * float(longitude)
        y = float(latitude) * 111

        # Считаем дистанцию без учета "большой окружности"
        return int(math.sqrt(x * x + y * y))

    @staticmethod
    def convert_weight(uom_code, cargo_weight=0):
        """
        Привести вес груза к килограммам
        """

        if not uom_code or uom_code not in ('163', '166', '168', '206'):
            return 0

        # Граммы
        cargo_weight = cargo_weight/Decimal('1000.00') if uom_code == '163' else cargo_weight

        # Центнер (метрический)
        cargo_weight = cargo_weight*Decimal('100.00') if uom_code == '206' else cargo_weight

        # Тонна метрическая
        cargo_weight = cargo_weight*Decimal('1000.00') if uom_code == '168' else cargo_weight

        return cargo_weight

    def calculate_delivery_cost(self, address_store, address_consignee, product_sku, quantity):
        total_delivery_cost = 0

        cargo_weight = 0  # Вес груза по-умолчанию нулевой
        try:
            product = Product.objects.get(sku=product_sku) if product_sku else None
        except ObjectDoesNotExist:
            pass
        else:
            try:
                weight_feature = product.additional_features.filter(
                    models.Q(name='gross_weight') | models.Q(name='net_weight') | models.Q(name='weight')
                )[0]
                cargo_weight = self.convert_weight(weight_feature.uom.code, Decimal(weight_feature.value))
            except ObjectDoesNotExist:
                # Если единицы измерения товара весовые,
                # то количество единиц товара и будет весом груза
                cargo_weight = self.convert_weight(product.uom.code, quantity)

        # Подбираем наиболее подходящую базовую цену доставки с учетом веса товара
        delivery_cost = 0  # Цена доставки по-умолчанию нулевая
        tmp = 0
        for position in self.delivery_price_list.order_by("max_weight_cargo"):
            if tmp < cargo_weight <= position.max_weight_cargo:
                delivery_cost = position.delivery_cost
                tmp = position.max_weight_cargo

        # Вычисляем дистанцию от розничного склада, до адреса грузополучателя
        distance = self.calculate_distance(address_store, address_consignee)

        # Подбираем наиболее подходящую зону и зональную наценку
        markup = 0  # Наценка по-умолчанию равна нулю
        tmp = 0
        for area in self.delivery_areas.order_by("distance"):
            if tmp < distance <= area.distance:
                markup = area.markup
                tmp = area.distance

        # Корректируем стоимость доставки с учетом зональности
        total_delivery_cost += (delivery_cost + markup)

        # Возвращаем полученный результат
        return total_delivery_cost

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.pk:
            # Устанавливаем справочный код
            s = Sequence.objects.get_or_create(
                name='carrier',
                defaults={
                    'name': 'carrier',
                    'prefix': '',
                    'separator': '',
                }
            )[0]
            self.code = s.request_next_number()

        super(Carrier, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return unicode(self.party)


class AvailablePaymentMethod(models.Model):
    """
    Доступный метод оплаты
    """

    class Meta:
        verbose_name = _('Available payment method')
        verbose_name_plural = _('Available payment methods')

    PAYMENT_METHODS = (
        ('prepayment', _('Prepayment')),
        ('cod', _('Cash on delivery'))
    )

    method = models.CharField(max_length=255, choices=PAYMENT_METHODS, verbose_name=_('Method'))
    carrier = models.ForeignKey(Carrier, related_name='payment_methods', verbose_name=_('Carrier'))


class DeliveryCost(models.Model):
    """
    Стоимость доставки
    """

    class Meta:
        verbose_name = _('Delivery cost')
        verbose_name_plural = _('Delivery costs')

    max_weight_cargo = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=False,
                                           verbose_name=_('Max weight cargo'),
                                           help_text=_('In kilograms'))
    delivery_cost = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=False,
                                        verbose_name=_('Delivery cost'))
    carrier = models.ForeignKey(Carrier, related_name='delivery_price_list', verbose_name=_('Carrier'))


class DeliveryArea(models.Model):
    """
    Зона доставки
    """

    class Meta:
        verbose_name = _('Delivery area')
        verbose_name_plural = _('Delivery areas')

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    distance = models.PositiveIntegerField(verbose_name=_('Distance'), help_text=_('In kilometers'))
    markup = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=_('Markup'))
    time = models.PositiveIntegerField(verbose_name=_('Time delivery'), help_text=_('In hours'))
    description = models.TextField(null=True, blank=True, verbose_name=_('Description'))
    carrier = models.ForeignKey(Carrier, related_name='delivery_areas', verbose_name=_('Carrier'))

    def __unicode__(self):
        return self.name


class Sequence(models.Model):
    class Meta:
        verbose_name = _('Sequence')
        verbose_name_plural = _('Sequences')

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    separator = models.CharField(default='-', blank=True, max_length=1, verbose_name=_('Separator'))
    prefix = models.CharField(max_length=8, null=True, blank=True, verbose_name=_('Prefix'))
    number = models.IntegerField(editable=False, default=0, verbose_name=_('Number'))
    suffix = models.CharField(max_length=8, null=True, blank=True, verbose_name=_('Suffix'))

    @transaction.atomic
    def request_next_number(self):
        s = Sequence.objects.select_for_update().get(id=self.id)
        return s.get_next_number()

    def get_next_number(self, force_insert=False, force_update=False, using=None):
        self.number += 1
        super(Sequence, self).save(force_insert, force_update, using)
        if self.prefix and self.suffix:
            return u"{0}{3}{1}{3}{2}".format(self.prefix, self.number, self.suffix, self.separator)
        if self.prefix:
            return u"{0}{2}{1}".format(self.prefix, self.number, self.separator)
        if self.suffix:
            return u"{0}{2}{1}".format(self.number, self.suffix, self.separator)
        return u"{0}".format(self.number)

    def reset_sequence(self, force_insert=False, force_update=False, using=None):
        self.number = 0
        super(Sequence, self).save(force_insert, force_update, using)

    def __unicode__(self):
        return self.name
