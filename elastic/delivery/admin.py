# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *
from django import forms

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Delivery package"


class AvailablePaymentMethodInline(admin.TabularInline):
    model = AvailablePaymentMethod


class DeliveryCostInline(admin.TabularInline):
    model = DeliveryCost


class DeliveryAreaInline(admin.TabularInline):
    model = DeliveryArea


class CarrierForm(forms.ModelForm):
    class Meta:
        model = Carrier
        fields = '__all__'

    code = forms.CharField(required=False)


class CarrierAdmin(admin.ModelAdmin):
    list_display = ('code', 'party', )
    form = CarrierForm
    inlines = [
        AvailablePaymentMethodInline,
        DeliveryCostInline,
        DeliveryAreaInline
    ]

admin.site.register(Carrier, CarrierAdmin)
