# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('party', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='AvailablePaymentMethod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('method', models.CharField(max_length=255, verbose_name='Method', choices=[(b'prepayment', 'Prepayment'), (b'cod', 'Cash on delivery')])),
            ],
            options={
                'verbose_name': 'Available payment method',
                'verbose_name_plural': 'Available payment methods',
            },
        ),
        migrations.CreateModel(
            name='Carrier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=64, verbose_name='Code', db_index=True)),
                ('party', models.ForeignKey(related_name='carriers', verbose_name='Party', to='party.Party')),
            ],
            options={
                'verbose_name': 'Carrier',
                'verbose_name_plural': 'Carriers',
            },
        ),
        migrations.CreateModel(
            name='DeliveryArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('distance', models.PositiveIntegerField(help_text='In kilometers', verbose_name='Distance')),
                ('markup', models.DecimalField(verbose_name='Markup', max_digits=15, decimal_places=2)),
                ('time', models.PositiveIntegerField(help_text='In hours', verbose_name='Time delivery')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('carrier', models.ForeignKey(related_name='delivery_areas', verbose_name='Carrier', to='delivery.Carrier')),
            ],
            options={
                'verbose_name': 'Delivery area',
                'verbose_name_plural': 'Delivery areas',
            },
        ),
        migrations.CreateModel(
            name='DeliveryCost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_weight_cargo', models.DecimalField(help_text='In kilograms', null=True, verbose_name='Max weight cargo', max_digits=15, decimal_places=2)),
                ('delivery_cost', models.DecimalField(null=True, verbose_name='Delivery cost', max_digits=15, decimal_places=2)),
                ('carrier', models.ForeignKey(related_name='delivery_price_list', verbose_name='Carrier', to='delivery.Carrier')),
            ],
            options={
                'verbose_name': 'Delivery cost',
                'verbose_name_plural': 'Delivery costs',
            },
        ),
        migrations.CreateModel(
            name='Sequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('separator', models.CharField(default=b'-', max_length=1, verbose_name='Separator', blank=True)),
                ('prefix', models.CharField(max_length=8, null=True, verbose_name='Prefix', blank=True)),
                ('number', models.IntegerField(default=0, verbose_name='Number', editable=False)),
                ('suffix', models.CharField(max_length=8, null=True, verbose_name='Suffix', blank=True)),
            ],
            options={
                'verbose_name': 'Sequence',
                'verbose_name_plural': 'Sequences',
            },
        ),
        migrations.AddField(
            model_name='availablepaymentmethod',
            name='carrier',
            field=models.ForeignKey(related_name='payment_methods', verbose_name='Carrier', to='delivery.Carrier'),
        ),
    ]
